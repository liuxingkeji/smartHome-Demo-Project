/********************************** (C) COPYRIGHT *******************************
* File Name          : app.c
* Author             : WCH
* Version            : V1.0.0
* Date               : 2021/06/06
* Description        : This file contains the headers of the OV2640.
*******************************************************************************/
#include "app.h"
#include "lcd.h"
#include "lcd_init.h"
#include "tos_k.h"
#include "tos_mutex.h"
#include "colorcfg.h"
#include "esp8266_tencent_firmware.h"
#include "tencent_firmware_module_wrapper.h"


#include "sht20.h"

#define camera_stk_size      1024
k_task_t camera_task;
__aligned(4) uint8_t camera_stk[camera_stk_size];


/* dvp ?锟斤拷????? */
__attribute__ ((aligned(4))) uint16_t DVP_LINE_ADDR0[DVP_CLOS] __attribute__((section(".DMADATA")));
__attribute__ ((aligned(4))) uint16_t DVP_LINE_ADDR1[DVP_CLOS] __attribute__((section(".DMADATA")));
/* rgb?????? */
__attribute__ ((aligned(4))) uint16_t rgbBuffer[DVP_CLOS*DVP_ROWS] __attribute__((section(".DMADATA")));


k_sem_t rgbdataReady;



/*********************************************************************
 * @fn      DVP_Init
 *
 * @brief   Init DVP
 *
 * @return  none
 */
void DVP_Init(void *buf0,void *buf1)
{
    NVIC_InitTypeDef NVIC_InitStructure={0};

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DVP, ENABLE);

    DVP->CR0 &= ~RB_DVP_MSK_DAT_MOD;

    /* VSYNC??HSYNC - High level active */
    DVP->CR0 |= RB_DVP_D8_MOD | RB_DVP_V_POLAR;
    DVP->CR1 &= ~((RB_DVP_ALL_CLR)| RB_DVP_RCV_CLR);
    DVP->ROW_NUM = DVP_ROWS;             // rows
    DVP->COL_NUM = (DVP_CLOS*2);         // cols

    DVP->DMA_BUF0 = (uint32_t)buf0;      //DMA addr0
    DVP->DMA_BUF1 = (uint32_t)buf1;      //DMA addr1

    /* Set frame capture rate */
    DVP->CR1 &= ~RB_DVP_FCRC;
    DVP->CR1 |= DVP_RATE_100P;  //100%

    //Interupt Enable
    DVP->IER |= RB_DVP_IE_STP_FRM;
    DVP->IER |= RB_DVP_IE_FIFO_OV;
    DVP->IER |= RB_DVP_IE_FRM_DONE;
    DVP->IER |= RB_DVP_IE_ROW_DONE;
    DVP->IER |= RB_DVP_IE_STR_FRM;

    NVIC_InitStructure.NVIC_IRQChannel = DVP_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    DVP->CR1 |= RB_DVP_DMA_EN;  //enable DMA
    DVP->CR0 |= RB_DVP_ENABLE;  //enable DVP
}

void color_assignment(void)
{

    COLOR_RGB_t rgb_tmp;
    COLOR_HLS_t hls_tmp;

    ReadColor(IMG_X+5, IMG_Y+5, &rgb_tmp);//??????rgb

    RGB2HSL( &rgb_tmp, &hls_tmp );//?????hsl

    condition[global_page].H_MIN=hls_tmp.Hue-COLOR_RANG;
    condition[global_page].H_MAX=hls_tmp.Hue+COLOR_RANG;

    condition[global_page].L_MIN=hls_tmp.Lightness-COLOR_RANG;
    condition[global_page].L_MAX=hls_tmp.Lightness+COLOR_RANG;

    condition[global_page].S_MIN=hls_tmp.Saturation-COLOR_RANG;
    condition[global_page].S_MAX=hls_tmp.Saturation+COLOR_RANG;

    condition[global_page].HEIGHT_MIN=15;
    condition[global_page].HEIGHT_MAX=120;

    condition[global_page].WIDTH_MAX=120;
    condition[global_page].WIDTH_MIN=15;


    printf("H_MIN??%d,H_MAM??%d\r\n",condition[global_page].H_MIN,condition[global_page].H_MAX);
    printf("S_MIN??%d,S_MAM??%d\r\n",condition[global_page].S_MIN,condition[global_page].S_MAX);
    printf("L_MIN??%d,S_MAM??%d\r\n",condition[global_page].L_MIN,condition[global_page].L_MAX);

    if(++global_page>=TRACE_NUM)
        global_page=0;
}


void key1_init(void)
{
  GPIO_InitTypeDef  GPIO_InitStructure={0};
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0; /* key1 */
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
}

extern k_sem_t status_change;
extern k_chr_fifo_t status_fifo;


u8 showCamera_flag = 0;

void camera_task_entry(void *arg)
{
    uint16_t i=0;
    uint16_t succ_count = 0, fail_count = 0;
    int dev_status = 0;

    while(OV2640_Init())
     {
         printf("Camera Model Err\r\n");
         tos_task_delay(1000);
     }
     tos_task_delay(1000);
     RGB565_Mode_Init();
     tos_task_delay(1000);

     printf("camera init OK\r\n");
     DVP_Init(DVP_LINE_ADDR0,DVP_LINE_ADDR1);

     tos_sem_create(&rgbdataReady, 0);

    while (1)
    {
        if(!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0))
        {
            tos_task_delay(20);
            if(!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0))
            {
                showCamera_flag = 1;
            }
        }
        if(showCamera_flag)
        {
            if(tos_sem_pend(&rgbdataReady, TOS_TIME_FOREVER) == K_ERR_NONE )
            {
                LCD_ShowImage(0,0,240,120,rgbBuffer);

    //            if(!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0))
    //            {
    //                tos_task_delay(20);
    //                if(!GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0))
    //                {
    //                    color_assignment();
    //                }
    //            }

    //            for(i=0;i<TRACE_NUM;i++)
    //             {
    //                 if(Trace(&condition[i], &result[i]))
    //                 {
    //                    LCD_DrawRectangle( result[i].x-result[i].w/2, result[i].y-result[i].h/2, result[i].x-result[i].w/2+result[i].w,  result[i].y-result[i].h/2+result[i].h,BLUE);
    //                    printf("[%d]success\r\n", i);
    //                    if (dev_status == 1) {
    //                        break;
    //                    }
    //                    succ_count++;
    //                    fail_count = 0;
    //                    if (succ_count > 10) {
    //                        succ_count = 0;
    //                        dev_status = 1;
    //                        printf("--------------->fault!\r\n");
    //                        tos_chr_fifo_push(&status_fifo, dev_status);
    //                        tos_sem_post(&status_change);
    //                    }
    //                 }
    //                 else
    //                 {
    //                     printf("[%d]fail\r\n", i);
    //                     if (dev_status == 0) {
    //                         break;
    //                     }
    //                     succ_count = 0;
    //                     fail_count++;
    //                     if (fail_count > 10) {
    //                         fail_count = 0;
    //                         dev_status = 0;
    //                         printf("--------------->normal!\r\n");
    //                         tos_chr_fifo_push(&status_fifo, dev_status);
    //                         tos_sem_post(&status_change);
    //                     }
    //                 }
    //             }

                DVP->CR1 |= RB_DVP_DMA_EN;  //enable DMA
                DVP->CR0 |= RB_DVP_ENABLE;  //enable DVP

            }
        }
        else {
            tos_task_delay(2000);
        }

    }
}

int camera_task_init(void)
{
    k_err_t err;

    err = tos_task_create(&camera_task, "came", camera_task_entry, NULL, 4, camera_stk, camera_stk_size, 0);

    return err == K_ERR_NONE ? 0 : -1;
}

extern int mqtt_report_task_init(void);

s8 connect_wifi_flag = -1;

void application_entry(void *arg)
{
    int ret;



    printf("CH32V307 Ready\r\n");


    ret = esp8266_tencent_firmware_sal_init(HAL_UART_PORT_2);
    if (ret < 0) {
        printf("esp8266 tencent firmware sal init fail, ret is %d\r\n", ret);
        return;
    }

    connect_wifi_flag = 0;

    while (esp8266_tencent_firmware_join_ap("mgh", "147258369") != 0) {
        printf("connect wifi... fail!\r\n");
        connect_wifi_flag = 1;
    }
    connect_wifi_flag = 2;
    printf("connect wifi... ok!\r\n");

    ret = mqtt_report_task_init();
    if (ret < 0) {
        printf("mqtt report task create fail!\r\n");
        return;
    }

    ret = camera_task_init();
    if (ret < 0) {
        printf("camera_task_init create fail!\r\n");
        return;
    }

}


volatile UINT32 addr_cnt = 0;
volatile UINT32 href_cnt = 0;
/*********************************************************************
 * @fn      DVP_IRQHandler
 *
 * @brief   This function handles DVP exception.
 *
 * @return  none
 */
void DVP_IRQHandler (void) __attribute__((interrupt("WCH-Interrupt-fast")));
void DVP_IRQHandler(void)
{
    u32 *pbuf;
    u32 *buf0=(u32*)DVP_LINE_ADDR0;
    u32 *buf1=(u32*)DVP_LINE_ADDR1;
    u16 i;

    if (DVP->IFR & RB_DVP_IF_ROW_DONE)
    {
        pbuf=(u32*)(rgbBuffer+href_cnt*DVP_CLOS);
//        printf("&%08x\n",pbuf);
        /* Write 0 clear 0 */
        DVP->IFR &= ~RB_DVP_IF_ROW_DONE;  //clear Interrupt
        if (addr_cnt%2)                   //buf1 done
        {
            addr_cnt++;
            for(i=0;i<(DVP_CLOS/2);i++)  //word by word
            {
                 pbuf[i]=buf1[i];
            }
        }
        else                             //buf0 done
        {
            addr_cnt++;
            for(i=0;i<(DVP_CLOS/2);i++)
            {
                pbuf[i]=buf0[i];
            }
        }
        href_cnt++;
    }

    if (DVP->IFR & RB_DVP_IF_FRM_DONE)
    {
        DVP->CR1 &= ~(RB_DVP_DMA_EN);  //disable DMA
        DVP->CR0 &= ~(RB_DVP_ENABLE);  //disable DVP

        DVP->IFR &= ~RB_DVP_IF_FRM_DONE;  //clear Interrupt

        addr_cnt = 0;
        href_cnt = 0;
        tos_sem_post(&rgbdataReady);
    }

    if (DVP->IFR & RB_DVP_IF_STR_FRM)
    {
        DVP->IFR &= ~RB_DVP_IF_STR_FRM;
    }

    if (DVP->IFR & RB_DVP_IF_STP_FRM)
    {
        DVP->IFR &= ~RB_DVP_IF_STP_FRM;
    }

    if (DVP->IFR & RB_DVP_IF_FIFO_OV)
    {
        DVP->IFR &= ~RB_DVP_IF_FIFO_OV;
    }
}

extern uint16_t PM1_0;
extern uint16_t PM2_5;
extern uint16_t PM10;

u8 AirConditionStatus = 0; //0默认是关 1是开
u8 AirConditionMode = 0; //0是制冷 1，是制热，2是除湿
u8 AirConditionSettingTemperature = 26; //默认温度是26度

void lcd_display_entry(void *arg)
{
    uint8_t cnt = 0;
    LCD_Init();
    key1_init();
    LCD_Fill(0,0,LCD_W,LCD_H,BLACK);

    while(1)
    {
        if(!showCamera_flag)
        {
            LCD_Fill(0,0,LCD_W,LCD_H,BLACK);

            //PM2.5激光传感器
            LCD_ShowString(0,0,"PM10:",WHITE,BLACK,16,0);
            LCD_ShowIntNum(40,0,PM10,3,WHITE,BLACK,16);
            LCD_ShowString(70,0,"ug/m3",WHITE,BLACK,16,0);

            LCD_ShowString(120,0,"PM1_0:",WHITE,BLACK,16,0);
            LCD_ShowIntNum(165,0,PM1_0,3,WHITE,BLACK,16);
            LCD_ShowString(195,0,"ug/m3",WHITE,BLACK,16,0);


            LCD_ShowString(0,20,"PM2_5:",WHITE,BLACK,16,0);
            LCD_ShowIntNum(50,20,PM2_5,3,WHITE,BLACK,16);
            LCD_ShowString(80,20,"ug/m3",WHITE,BLACK,16,0);

            if(PM2_5<35)
            {
                LCD_ShowString(130,20,"excellent",GREEN,BLACK,16,0);
            }
            else if(PM2_5>35&&PM2_5<75)
            {
                LCD_ShowString(140,20,"good",LIGHTGREEN,BLACK,16,0);
            }
            else if(PM2_5>75&&PM2_5<115)
            {
                LCD_ShowString(120,20,"mild contamination",BROWN,BLACK,16,0);
            }
            else if(PM2_5>115&&PM2_5<150)
            {
                LCD_ShowString(120,20,"medium pollution",BROWN,BLACK,16,0);
            }
            else if(PM2_5>150)
            {
                LCD_ShowString(120,20,"heavy pollution",BRRED,BLACK,16,0);
            }

            //sgp30传感器
            LCD_ShowString(0,40,"TVOC:",WHITE,BLACK,16,0);
            LCD_ShowFloatNum1(40,40,1.7,3,WHITE,BLACK,16);
            LCD_ShowString(80,40,"ppd",WHITE,BLACK,16,0);

            LCD_ShowString(120,40,"CO2:",WHITE,BLACK,16,0);
            LCD_ShowFloatNum1(150,40,1.4,3,WHITE,BLACK,16);
            LCD_ShowString(185,40,"ppm",WHITE,BLACK,16,0);

            //温湿度显示
            LCD_ShowString(0,60,"TEMP:",WHITE,BLACK,16,0);
            LCD_ShowFloatNum1(55,60,sht20Info.tempreture,4,WHITE,BLACK,16);
            LCD_ShowString(100,60,"C",WHITE,BLACK,16,0);

            LCD_ShowString(120,60,"HUMI:",WHITE,BLACK,16,0);
            LCD_ShowFloatNum1(165,60,sht20Info.humidity,4,WHITE,BLACK,16);
            LCD_ShowString(210,60,"%",WHITE,BLACK,16,0);

            LCD_ShowChinese(0,90,"空调状态",WHITE,BLACK,16,0);
            LCD_ShowString(100,90,":",WHITE,BLACK,16,0);
            if(AirConditionStatus)
            {
                LCD_ShowChinese(120,90,"开",WHITE,BLACK,16,0);
            }
            else {
                LCD_ShowChinese(120,90,"关",WHITE,BLACK,16,0);
            }


            LCD_ShowChinese(0,110,"空调模式",WHITE,BLACK,16,0);
            LCD_ShowString(100,110,":",WHITE,BLACK,16,0);

            if(AirConditionMode == 0)
            {
                LCD_ShowChinese(120,110,"制冷",WHITE,BLACK,16,0);
            }
            else if (AirConditionMode == 1) {
                LCD_ShowChinese(120,110,"制热",WHITE,BLACK,16,0);
            }
            else if (AirConditionMode == 2) {
                LCD_ShowChinese(120,110,"除湿",WHITE,BLACK,16,0);
            }


            LCD_ShowChinese(0,130,"设定温度",WHITE,BLACK,16,0);
            LCD_ShowString(100,130,":",WHITE,BLACK,16,0);
            LCD_ShowIntNum(120,130,AirConditionSettingTemperature,2,WHITE,BLACK,16);
            LCD_ShowChinese(140,130,"℃",WHITE,BLACK,16,0);
            //连接wifi显示
            if(connect_wifi_flag == 0)
            {
                LCD_ShowString(30,140+16+16+16,"connect wifi...",WHITE,BLACK,16,0);
            }
            else if(connect_wifi_flag == 1)
            {
                LCD_ShowString(30,140+16+16+16,"connect wifi fail",WHITE,BLACK,16,0);
            }
            else if(connect_wifi_flag == 2)
            {
                connect_wifi_flag = -1;
                LCD_ShowString(30,140+16+16+16,"connect wifi ok",WHITE,BLACK,16,0);
            }

            tos_task_delay(3000);
        }
        else
        {
           if(cnt>5)
           {
               cnt = 0;
//               LCD_Fill(0,0,LCD_W,LCD_H,BLACK);
               showCamera_flag = 0;
           }
           tos_task_delay(1000);
           cnt++;

        }


    }
}


