/********************************** (C) COPYRIGHT *******************************
* File Name          : ov.h
* Author             : WCH
* Version            : V1.0.0
* Date               : 2021/06/06
* Description        : This file contains the headers of the OV2640.
*******************************************************************************/
#ifndef __APP_H
#define __APP_H

#include "debug.h"
#include "ov.h"


/* 一个buffer空间大小240*120*2  */
#define DVP_CLOS  240     //宽
#define DVP_ROWS  120     //高

extern __attribute__ ((aligned(4))) uint16_t rgbBuffer[DVP_CLOS*DVP_ROWS] __attribute__((section(".DMADATA")));

void app_peripheral_init(void);
int camera_task_init(void);

#endif

