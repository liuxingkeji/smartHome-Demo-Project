#ifndef _I2C_H_
#define _I2C_H_

#include "ch32v30x.h"


#define IIC_OK		0
#define IIC_Err		1



//SDA		PB11
//SCL		PB10
#define SDA_H	GPIO_WriteBit(GPIOE, GPIO_Pin_15,Bit_SET)
#define SDA_L	GPIO_WriteBit(GPIOE, GPIO_Pin_15,Bit_RESET)
#define SDA_R	GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_15)

#define SCL_H	GPIO_WriteBit(GPIOE, GPIO_Pin_14,Bit_SET)
#define SCL_L	GPIO_WriteBit(GPIOE, GPIO_Pin_14,Bit_RESET)


typedef struct
{

	unsigned short speed;

} IIC_INFO;

extern IIC_INFO iicInfo;




void IIC_Init(void);

void IIC_SpeedCtl(unsigned short speed);

_Bool I2C_WriteByte(unsigned char slaveAddr, unsigned char regAddr, unsigned char *byte);

_Bool I2C_ReadByte(unsigned char slaveAddr, unsigned char regAddr, unsigned char *val);

_Bool I2C_WriteBytes(unsigned char slaveAddr, unsigned char regAddr, unsigned char *buf, unsigned char num);

_Bool I2C_ReadBytes(unsigned char slaveAddr, unsigned char regAddr, unsigned char *buf, unsigned char num);

void IIC_Start(void);

void IIC_Stop(void);

_Bool IIC_WaitAck(unsigned int timeOut);

void IIC_Ack(void);

void IIC_NAck(void);

void IIC_SendByte(unsigned char byte);

unsigned char IIC_RecvByte(void);


#endif
