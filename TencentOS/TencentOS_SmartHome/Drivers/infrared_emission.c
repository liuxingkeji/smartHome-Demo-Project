/*
 * infrared_emission.c
 *
 *  Created on: Aug 9, 2022
 *      Author: XI
 */
#include "infrared_emission.h"
/* PWM Output Mode Definition */
#define PWM_MODE1   0
#define PWM_MODE2   1

/* PWM Output Mode Selection */
//#define PWM_MODE PWM_MODE1
#define PWM_MODE PWM_MODE2

#define CARRIER_38KHz() TIM_CtrlPWMOutputs(TIM9, ENABLE )//产生方波
#define NO_CARRIER()    TIM_CtrlPWMOutputs(TIM9, DISABLE )//无波形

uint8_t AUX_OFF[13]={0xC3,0x97,0xE0,0x00,0xA0,0x00,0x20,0x00,0x00,0x00,0x00,0x05,0x00};

uint8_t AUX_ON[13]={0xC3,0x97,0xE0,0x00,0xA0,0x00,0x20,0x00,0x00,0x20,0x00,0x05,0x00};

void infrared_emission_IO_Init(u16 arr, u16 psc, u16 ccp )
{

    GPIO_InitTypeDef GPIO_InitStructure={0};
    TIM_OCInitTypeDef TIM_OCInitStructure={0};
    TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure={0};

    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOC | RCC_APB2Periph_TIM9, ENABLE );

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init( GPIOC, &GPIO_InitStructure );

    TIM_TimeBaseInitStructure.TIM_Period = arr;
    TIM_TimeBaseInitStructure.TIM_Prescaler = psc;
    TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
    TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit( TIM9, &TIM_TimeBaseInitStructure);

#if (PWM_MODE == PWM_MODE1)
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;

#elif (PWM_MODE == PWM_MODE2)
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM2;

#endif

    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = ccp;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_Low;
    TIM_OC4Init( TIM9, &TIM_OCInitStructure );

//    TIM_CtrlPWMOutputs(TIM9, ENABLE );
    TIM_OC1PreloadConfig( TIM9, TIM_OCPreload_Disable );
    TIM_ARRPreloadConfig( TIM9, ENABLE );
    TIM_Cmd( TIM9, ENABLE );

}

void Infrared_Send(uint8_t *s,int n,uint8_t cnt)
{
   uint8_t i,j,temp;

    CARRIER_38KHz();
    Delay_Us(90);
    Delay_Us(90);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);

    NO_CARRIER();
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(40);
    Delay_Us(40);
    Delay_Us(40);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);
    Delay_Us(400);


    for(i=0;i<n;i++)
    {
        for(j=0;j<8;j++)
          {
             temp=(s[i]>>j)&0x01;
             if(temp==0)//发射0
             {
                CARRIER_38KHz();
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(150);//延时0.5ms
                NO_CARRIER();
                Delay_Us(150);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms

             }
             if(temp==1)//发射1
             {
                CARRIER_38KHz();
                Delay_Us(150);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                Delay_Us(50);//延时0.5ms
                NO_CARRIER();
                Delay_Us(169);//延时1.69ms  690
                Delay_Us(169);//延时1.69ms  690
                Delay_Us(169);//延时1.69ms  690
                Delay_Us(169);//延时1.69ms  690
                Delay_Us(169);//延时1.69ms  690
                Delay_Us(169);//延时1.69ms  690
                Delay_Us(169);//延时1.69ms  690
                Delay_Us(169);//延时1.69ms  690
                Delay_Us(169);//延时1.69ms  690
                Delay_Us(150);//延时1.69ms  690
              }
          }
    }
    CARRIER_38KHz();//结束
    Delay_Us(90);//延时0.56ms
    Delay_Us(90);//延时0.56ms
    Delay_Us(90);//延时0.56ms
    Delay_Us(90);//延时0.56ms
    Delay_Us(90);//延时0.56ms
    Delay_Us(90);//延时0.56ms
    NO_CARRIER(); //关闭红外发射
}


void AirConditionerON(void)
{
    uint8_t i;
    uint8_t checksum = 0;
    uint8_t checksum_Two = 0;
    for (i = 0; i < 12; i++)
    {
        checksum += AUX_ON[i];
    }
    AUX_ON[12] = checksum;

    Infrared_Send(AUX_ON,13,0);
}

void AirConditionerOFF(void)
{

    uint8_t i;
    uint8_t checksum = 0;
    uint8_t checksum_Two = 0;
    for (i = 0; i < 12; i++)
    {
        checksum += AUX_OFF[i];
    }
    AUX_OFF[12] = checksum;

    Infrared_Send(AUX_OFF,13,0);
}

