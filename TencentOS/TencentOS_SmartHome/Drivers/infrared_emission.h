/*
 * infrared_emission.h
 *
 *  Created on: Aug 9, 2022
 *      Author: XI
 */

#ifndef INFRARED_EMISSION_H_
#define INFRARED_EMISSION_H_


#include "ch32v30x.h"

void infrared_emission_IO_Init(u16 arr, u16 psc, u16 ccp);

void AirConditionerON(void);
void AirConditionerOFF(void);

#endif /* DRIVERS_INFRARED_EMISSION_H_ */
