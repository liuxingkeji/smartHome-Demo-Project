#include "tos_k.h"
#include "esp8266_tencent_firmware.h"
#include "tencent_firmware_module_wrapper.h"
#include "sht20.h"

#include "cJSON.h"

#include "infrared_emission.h"

#define PRODUCT_ID              "QEAA1FI1TZ"
#define DEVICE_NAME             "CH32V307"
#define DEVICE_KEY              "wX1Yu6yTv+xOjozoxc/jeA=="

#define REPORT_DATA_TEMPLATE    "{\\\"method\\\":\\\"report\\\"\\,\\\"clientToken\\\":\\\"00000001\\\"\\,\\\"params\\\":{\\\"Temperature\\\":\\\"%.2f\\\"\\,\\\"CO2\\\":\\\"%.2f\\\"\\,\\\"Humidity\\\":\\\"%.2f\\\"\\,\\\"TVOC\\\":\\\"%.2f\\\"}}"

#define REPORT_DATA_AIRCONDITIONING "{\\\"method\\\":\\\"report\\\"\\,\\\"clientToken\\\":\\\"00000001\\\"\\,\\\"params\\\":{\\\"AirConditioningSwitch\\\":\\\"%d\\\"\\,\\\"AirConditioningMode\\\":\\\"%d\\\"\\,\\\"TemperatureSetting\\\":\\\"%d\\\"}}"


#define REPORT_DATA_PM "{\\\"method\\\":\\\"report\\\"\\,\\\"clientToken\\\":\\\"00000001\\\"\\,\\\"params\\\":{\\\"PM2_5\\\":\\\"%d\\\"\\,\\\"PM10\\\":\\\"%d\\\"\\,\\\"PM1_0\\\":\\\"%d\\\"}}"


#define MQTT_REPORT_TASK_STK_SIZE       4096
k_task_t mqtt_report_task;
__aligned(4) uint8_t mqtt_report_task_stk[MQTT_REPORT_TASK_STK_SIZE];

k_sem_t status_change;
k_chr_fifo_t status_fifo;
static char status_fifo_buf[128];


k_event_flag_t report_fail    = 1<<1;
k_event_flag_t report_success = 1<<0;


extern u8 AirConditionStatus; //0默认是关 1是开
extern u8 AirConditionMode; //0是制冷 1，是制热，2是除湿
extern u8 AirConditionSettingTemperature; //默认温度是26度

void default_message_handler(mqtt_message_t* msg)
{
    printf("callback:\r\n");
    printf("---------------------------------------------------------\r\n");
    printf("\ttopic:%s\r\n", msg->topic);
    printf("\tpayload:%s\r\n", msg->payload);
    printf("---------------------------------------------------------\r\n");

    cJSON* cjson_root   = NULL;
    cJSON* cjson_method = NULL;
    cJSON* cjson_status = NULL;
    cJSON* cjson_params = NULL;
    cJSON* cjson_switch = NULL;
    cJSON* cjson_mode = NULL;
    cJSON* cjson_settemp = NULL;

    char* status = NULL;
    char* method = NULL;

    k_event_flag_t event_flag = report_fail;


/* 使用cjson解析上报响应数据 */
   cjson_root = cJSON_Parse(msg->payload+1);
   if (cjson_root == NULL) {
       printf("report reply message parser fail1\r\n");
       event_flag = report_fail;
       goto exit;
   }

   /* 提取method */
   cjson_method = cJSON_GetObjectItem(cjson_root, "method");
   method = cJSON_GetStringValue(cjson_method);
   if (cjson_method == NULL || method == NULL) {
       printf("report reply status parser fail2\r\n");
       event_flag = report_fail;
       goto exit;
   }

   if (strstr(method, "control"))
   {
       cjson_params = cJSON_GetObjectItem(cjson_root, "params");
       cjson_switch = cJSON_GetObjectItem(cjson_params, "AirConditioningSwitch");
       cjson_mode = cJSON_GetObjectItem(cjson_params, "AirConditioningMode");
       cjson_settemp = cJSON_GetObjectItem(cjson_params, "TemperatureSetting");

       if (cjson_params == NULL )
       {
          printf("control data parser fail4\r\n");
          cJSON_Delete(cjson_root);
          return;
      }

       if(cjson_switch != NULL)
       {
            AirConditionStatus = cjson_switch->valueint;
            if(AirConditionStatus)
            {
                AirConditionerON();
            }
            else
            {
                AirConditionerOFF();
            }
       }
        if(cjson_settemp != NULL)
        {
            AirConditionSettingTemperature = cjson_settemp->valueint;
            AUX_ON[1] = ((0x40>>3)+(AirConditionSettingTemperature-16))<<3|0x07;
            AUX_OFF[1] =  AUX_ON[1] ;
            if(AirConditionStatus)
            {
                AirConditionerON();
            }

        }

        if(cjson_mode != NULL)
        {
            AirConditionMode = cjson_mode->valueint;
           if(AirConditionMode == 0)
           {
               AUX_ON[6] = 0x20 ;
               AUX_OFF[6] = 0x20;
               if(AirConditionStatus)
               {
                   AirConditionerON();
               }

           }
           else if(AirConditionMode == 1)
           {
               AUX_ON[6] = 0x80 ;
               AUX_OFF[6] = 0x80;
               if(AirConditionStatus)
              {
                  AirConditionerON();
              }
           }
           else if(AirConditionMode == 2)
           {
               AUX_ON[6] = 0x40;
               AUX_OFF[6] = 0x40;
               if(AirConditionStatus)
                 {
                     AirConditionerON();
                 }

           }
        }






       cJSON_Delete(cjson_root);
       return;
   }

exit:
   cJSON_Delete(cjson_root);
   cjson_root = NULL;
   status = NULL;
   method = NULL;
}

char payload[800] = {0};
static char report_topic_name[TOPIC_NAME_MAX_SIZE] = {0};
static char report_reply_topic_name[TOPIC_NAME_MAX_SIZE] = {0};


extern uint16_t PM1_0;
extern uint16_t PM2_5;
extern uint16_t PM10;

void mqtt_report_task_entry(void *arg)
{
    int size = 0;
    mqtt_state_t state;
    char dev_status;
    k_err_t err;

    char *product_id = PRODUCT_ID;
    char *device_name = DEVICE_NAME;
    char *key = DEVICE_KEY;

    device_info_t dev_info;
    memset(&dev_info, 0, sizeof(device_info_t));

    strncpy(dev_info.product_id, product_id, PRODUCT_ID_MAX_SIZE);
    strncpy(dev_info.device_name, device_name, DEVICE_NAME_MAX_SIZE);
    strncpy(dev_info.device_serc, key, DEVICE_SERC_MAX_SIZE);
    tos_tf_module_info_set(&dev_info, TLS_MODE_PSK);

    mqtt_param_t init_params = DEFAULT_MQTT_PARAMS;
    if (tos_tf_module_mqtt_conn(init_params) != 0) {
        printf("module mqtt conn fail\n");
    } else {
        printf("module mqtt conn success\n");
    }

    if (tos_tf_module_mqtt_state_get(&state) != -1) {
        printf("MQTT: %s\n", state == MQTT_STATE_CONNECTED ? "CONNECTED" : "DISCONNECTED");
    }

    size = snprintf(report_reply_topic_name, TOPIC_NAME_MAX_SIZE, "$thing/down/property/%s/%s", product_id, device_name);

    if (size < 0 || size > sizeof(report_reply_topic_name) - 1) {
        printf("sub topic content length not enough! content size:%d  buf size:%d", size, (int)sizeof(report_reply_topic_name));
    }
    if (tos_tf_module_mqtt_sub(report_reply_topic_name, QOS0, default_message_handler) != 0) {
        printf("module mqtt sub fail\n");
    } else {
        printf("module mqtt sub success\n");
    }

    memset(report_topic_name, 0, sizeof(report_topic_name));
    size = snprintf(report_topic_name, TOPIC_NAME_MAX_SIZE, "$thing/up/property/%s/%s", product_id, device_name);

    if (size < 0 || size > sizeof(report_topic_name) - 1) {
        printf("pub topic content length not enough! content size:%d  buf size:%d", size, (int)sizeof(report_topic_name));
    }

    while (1) {


        snprintf(payload, sizeof(payload), REPORT_DATA_TEMPLATE, sht20Info.tempreture,1.4,sht20Info.humidity,1.7);


        if (tos_tf_module_mqtt_pub(report_topic_name, QOS0, payload) != 0) {
            printf("module mqtt pub fail\n");
            break;
        } else {
            printf("module mqtt pub success\n");
        }

        snprintf(payload, sizeof(payload), REPORT_DATA_AIRCONDITIONING, AirConditionStatus,AirConditionMode,AirConditionSettingTemperature);

        if (tos_tf_module_mqtt_pub(report_topic_name, QOS0, payload) != 0) {
            printf("module mqtt pub fail\n");
            break;
        } else {
            printf("module mqtt pub success\n");
        }

        snprintf(payload, sizeof(payload), REPORT_DATA_PM, PM2_5,PM10,PM1_0);

        if (tos_tf_module_mqtt_pub(report_topic_name, QOS0, payload) != 0) {
            printf("module mqtt pub fail\n");
            break;
        } else {
            printf("module mqtt pub success\n");
        }

        tos_task_delay(3000);
    }
}

int mqtt_report_task_init(void)
{
   k_err_t err;

   err = tos_sem_create(&status_change, 0);
   if (err != K_ERR_NONE) {
       printf("status change sem create fail!\r\n");
       return -1;
   }

   err = tos_chr_fifo_create(&status_fifo, status_fifo_buf, sizeof(status_fifo_buf));
   if (err != K_ERR_NONE) {
      printf("status fifo create fail!\r\n");
      return -1;
   }

   err = tos_task_create(&mqtt_report_task, "mqtt_report_task", mqtt_report_task_entry, NULL, 4, mqtt_report_task_stk, MQTT_REPORT_TASK_STK_SIZE, 0);

   return err == K_ERR_NONE ? 0 : -1;
}
