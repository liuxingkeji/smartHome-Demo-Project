/********************************** (C) COPYRIGHT *******************************
* File Name          : ch32v30x_it.c
* Author             : WCH
* Version            : V1.0.0
* Date               : 2021/06/06
* Description        : Main Interrupt Service Routines.
* Copyright (c) 2021 Nanjing Qinheng Microelectronics Co., Ltd.
* SPDX-License-Identifier: Apache-2.0
*******************************************************************************/
#include "ch32v30x_it.h"
#include "tos_at.h"

extern at_agent_t esp8266_tf_agent;

void NMI_Handler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void HardFault_Handler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void USART2_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));
void UART7_IRQHandler(void) __attribute__((interrupt("WCH-Interrupt-fast")));


/*********************************************************************
 * @fn      NMI_Handler
 *
 * @brief   This function handles NMI exception.
 *
 * @return  none
 */
void NMI_Handler(void)
{
}

/*********************************************************************
 * @fn      HardFault_Handler
 *
 * @brief   This function handles Hard Fault exception.
 *
 * @return  none
 */
void HardFault_Handler(void)
{
    printf("mcause:0x%08x\n",__get_MCAUSE());
    printf("mtval:0x%08x\n",__get_MTVAL());
    printf("mepc:0x%08x\n",__get_MEPC());
  while (1)
  {
  }
}

void USART2_IRQHandler(void)
{
  uint8_t data;
  if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
  {
      data= USART_ReceiveData(USART2);
      tos_at_uart_input_byte(&esp8266_tf_agent,data);
  }

}

u8 Uart7_RX_CNT=0;
u8 Uart7_RX_Buff[64];
u8 Uart7_ReceiveComplete_flag = 0;
void UART7_IRQHandler(void)
{
    uint8_t data;
    if(USART_GetITStatus(UART7, USART_IT_RXNE) != RESET)
    {
        data= USART_ReceiveData(UART7);
        Uart7_RX_Buff[Uart7_RX_CNT] =data;
        Uart7_RX_CNT++;
//        printf("Uart7_RX_CNT:%d\r\n",Uart7_RX_CNT);
        if(Uart7_RX_CNT>32)
        {

            Uart7_ReceiveComplete_flag = 1;
        }
    }

}
