/********************************** (C) COPYRIGHT *******************************
* File Name          : main.c
* Author             : WCH
* Version            : V1.0.0
* Date               : 2021/06/06
* Description        : Main program body.
* Copyright (c) 2021 Nanjing Qinheng Microelectronics Co., Ltd.
* SPDX-License-Identifier: Apache-2.0
*******************************************************************************/

#include "debug.h"
#include "tos_k.h"
#include "app.h"
#include "i2c.h"
#include "sht20.h"
/* Global define */

#include "infrared_emission.h"

/* Global Variable */
#define TASK1_STK_SIZE       1024
k_task_t task1;
__aligned(4) uint8_t task1_stk[TASK1_STK_SIZE];


#define UART7_RECEIVE_STK_SIZE       1024
k_task_t uart7_receivetask;
__aligned(4) uint8_t uart7_receive_stk[UART7_RECEIVE_STK_SIZE];

#define APPLICATION_TASK_STK_SIZE       4096
k_task_t application_task;
__aligned(4) uint8_t application_task_stk[APPLICATION_TASK_STK_SIZE];

extern void application_entry(void *arg);


#define LCD_DISPLAY_TASK_STK_SIZE       2048
k_task_t lcd_display_task;
__aligned(4) uint8_t lcd_display_task_stk[LCD_DISPLAY_TASK_STK_SIZE];

extern void lcd_display_entry(void *arg);

void task1_entry(void *arg)
{

    IIC_Init();
    while (1)
    {
        printf("###I am task1\r\n");
//        AirConditionerON();
        SHT20_GetValue();
        tos_task_delay(5000);
    }
}
extern u8 Uart7_RX_CNT;
extern u8 Uart7_RX_Buff[32];
extern u8 Uart7_ReceiveComplete_flag;

uint16_t PM1_0  = 0;
uint16_t PM2_5  = 0;
uint16_t PM10  = 0;

void uart7_receive_entry(void *arg)
{
    uart7_init(9600);

    u16 checkSum = 0;
    while (1)
    {

        if(Uart7_ReceiveComplete_flag)
        {
            if(Uart7_RX_Buff[0]==0x32 && Uart7_RX_Buff[1]==0x3D)
            {
                for(int i = 0; i<30;i++)
                {
                    checkSum +=Uart7_RX_Buff[i];
                }
                if(Uart7_RX_Buff[30]<<8|Uart7_RX_Buff[31] == checkSum)
                {
                    if(Uart7_RX_Buff[2]<<8|Uart7_RX_Buff[3] == 28)
                    {
                        PM1_0 = Uart7_RX_Buff[4]<<8|Uart7_RX_Buff[5];
                        PM2_5 = Uart7_RX_Buff[6]<<8|Uart7_RX_Buff[7];
                        PM10 = Uart7_RX_Buff[8]<<8|Uart7_RX_Buff[9];
                        printf("PM1_0:%d\r\n",PM1_0);
                        printf("PM2_5:%d\r\n",PM2_5);
                        printf("PM10:%d\r\n",PM10);
                    }
                }
                checkSum = 0;

            }
            Uart7_ReceiveComplete_flag = 0;
            Uart7_RX_CNT = 0;
        }
        tos_task_delay(1000);
    }
}


/*********************************************************************
 * @fn      main
 *
 * @brief   Main program.
 *
 * @return  none
 */
int main(void)
{
	USART_Printf_Init(115200);

	Delay_Init();

	infrared_emission_IO_Init(99, 38-1, 50);

	printf("SystemClk:%d\r\n",SystemCoreClock);
	printf("Welcome to TencentOS tiny(%s)\r\n", TOS_VERSION);
//	app_peripheral_init();
    tos_knl_init();

    tos_task_create(&lcd_display_task, "lcd_display_task", lcd_display_entry, NULL, 2, lcd_display_task_stk, LCD_DISPLAY_TASK_STK_SIZE, 0);
    tos_task_create(&task1, "task1", task1_entry, NULL, 2, task1_stk, TASK1_STK_SIZE, 0); // Create task1
    tos_task_create(&uart7_receivetask, "uart7_receivetask", uart7_receive_entry, NULL, 5, uart7_receive_stk, UART7_RECEIVE_STK_SIZE, 0);
    tos_task_create(&application_task, "application_task", application_entry, NULL, 3, application_task_stk, APPLICATION_TASK_STK_SIZE, 0);

    tos_knl_start();

    printf("should not run at here!\r\n");

    while(1)
	{
	    asm("nop");
	}
}
